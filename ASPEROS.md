# ASPEROS ISO

My own OS. Guide incomplete.

## Prerequisites

```bash
curl -X GET "http://ftp.spline.inf.fu-berlin.de/mirrors/archlinux/iso/2019.03.01/archlinux-2019.03.01-x86_64.iso" > archlinux-2019.03.01-x86_64.iso
pacman -S squashfs-tools arch-install-scripts dosfstools
```

## Mounting, unsquash and arch-chroot 

```bash
mkdir /mnt/archiso
mount -t iso9660 -o loop archlinux-2019.03.01-x86_64.iso /mnt/archiso/
cp -a /mnt/archiso asperos
cd asperos/arch/x86_64/
unsquashfs airootfs.sfs
cp ../boot/x86_64/vmlinuz squashfs-root/boot/vmlinuz-linux
arch-chroot squashfs-root /bin/bash
```

## Arch Installation Routine

### Language and Keymaps

```bash
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
timedatectl set-ntp true
hwclock --systohc
nano /etc/locale.gen

    en_US.UTF-8 UTF-8
    de_DE-UTF8 UTF-8

locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "KEYMAP=de-latin1" > /etc/vconsole.conf
```

## Network

```bash
echo "asperos" > /etc/hostname
echo "127.0.0.1	localhost" > /etc/hosts
echo "::1	localhost" >> /etc/hosts
echo "127.0.1.1	asperos.localdomain asperos" > /etc/hosts
```

## Security

```bash
nano /etc/ssh/sshd_config

    Port 22000
    PermitRootLogin no
```

### Skeleton

#### Pretty User

```bash
nano /etc/skel/.bashrc

#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '
alias exit="history -c && echo "" > ~/.bash_history && exit;";
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias la='ls -la --color=auto'
alias ls='ls --color=auto'

export LESSOPEN="| /usr/bin/source-highlight-esc.sh %s"
export LESS='-R '

nano /etc/pacman.conf

    Color
```

#### Hooks and Loaders TODO: wrong

```bash
cd /root
mkdir -p skel/pacman.d/hooks
nano skel/pacman.d/hooks/systemd-boot.hook

    [Trigger]
    Type = Package
    Operation = Upgrade
    Target = systemd
    
    [Action]
    Description = Updating systemd-boot...
    When = PostTransaction
    Exec = /usr/bin/bootctl update

mkdir -p skel/boot/loader
nano skel/boot/loader/loader.conf

    default asperos
    timeout 0
    editor 0
```

### Pacman

```bash
pacman-key --init
pacman-key --populate archlinux
pacman -Syu --force archiso linux
```

### Kernel

```bash
nano /etc/mkinitcpio.conf

    MODULES=(amdgpu)
    HOOKS=(base udev memdisk archiso_shutdown archiso archiso_loop_mnt archiso_pxe_common archiso_pxe_nbd archiso_pxe_http archiso_pxe_nfs archiso_kms block filesystems keyboard)

mkinitcpio -p linux
```

### Binaries

```bash
pacman -S intel-ucode libinput gnome gnome-tweak-tool xorg-server-xwayland networkmanager alsa-utils bluez bluez-utils cups cups-pdf dhclient network-manager-applet evolution seahorse vlc x265 libmpeg2 libdvdcss xvidcore youtube-dl ldns base-devel devtools cmake git source-highlight

    // gnome packages
    1 3 5 6 7 8 9 11 12 13 17 19 23 26 30 31 33 34 36 42 44 45 46 52 53 54 55 58 60 61 63
    // base-devel
    (defaults)
```

### Configuration

```bash
echo "XDG_SESSION_TYPE=wayland gnome-session" > /usr/local/bin/startw
chmod 755 /usr/local/bin/startw

mkdir -p /etc/skel/.local/share/gnome-shell/extensions
cd /etc/skel/.local/share/gnome-shell/extensions
git clone https://github.com/micheleg/dash-to-dock.git
cd dash-to-dock
make
make install

systemctl enable gdm
systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable org.cups.cupsd.service 

echo "Welcome to dannyos" > /etc/motd
echo "1. Change the root password! (# passwd)" > /etc/motd
echo "2. Create a new user (# useradd {user_name})" > /etc/motd
echo "3. Change user password (# passwd {user_name})" > /etc/motd
echo "4. Exit (exit)" > /etc/motd
echo "5. login with user credentials" > /etc/motd
echo "6. Start Gnome($ startw)" > /etc/motd
```

### Remaster ASPEROS

```bash
LANG=C pacman -Sl | awk '/\[installed\]$/ {print $1 "/" $2 "-" $3}' > /pkglist.txt
pacman -Scc

    y
    Y (default)

exit

mv squashfs-root/boot/vmlinuz-linux ~/asperos/arch/boot/x86_64/vmlinuz
mv squashfs-root/boot/initramfs-linux.img ~/asperos/arch/boot/x86_64/archiso.img
rm squashfs-root/boot/initramfs-linux-fallback.img
mv squashfs-root/pkglist.txt ~/asperos/arch/pkglist.x86_64.txt
rm airootfs.sfs
mksquashfs squashfs-root airootfs.sfs
mv squashfs-root ~/squashfs-root-bak
sha512sum airootfs.sfs > airootfs.sha512

cd ~

mkdir mnt
mount -t vfat -o loop ~/asperos/EFI/archiso/efiboot.img mnt
mkdir -p mnt/EFI/archiso
cp ~/asperos/arch/boot/x86_64/vmlinuz mnt/EFI/archiso/vmlinuz.efi 
cp ~/asperos/arch/boot/x86_64/archiso.img mnt/EFI/archiso/archiso.img

cd asperos

genisoimage -l -r -J -V "ARCH_201903" -b isolinux/isolinux.bin -no-emul-boot -boot-load-size 4 -boot-info-table -c isolinux/boot.cat -o ../asperos.iso ./

iso_label="ARCH_201903"

xorriso -as mkisofs -iso-level 3 -full-iso9660-filenames -volid "${iso_label}" -eltorito-boot isolinux/isolinux.bin -eltorito-catalog isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -isohybrid-mbr isolinux/isohdpfx.bin -eltorito-alt-boot -e EFI/archiso/efiboot.img -no-emul-boot -isohybrid-gpt-basdat -output asperos.iso ~/asperos

REPEAT IF WITH ERRORS

dd if=asperos.iso bs=512 count=1 of=~/asperos/isolinux/isohdpfx.bin
umount /run/media/genom/ARCH_201903/
dd bs=4M if=asperos.iso of=/dev/sdx status=progress oflag=sync

```

# INSTALLATION

```bash
boot sda1 EFI system partition 256 M
/    sda2 Linux x86-64 root (/) +

fdisk /dev/sda
mkfs.ext4 /dev/sda1

mount /dev/sda2 /mnt
mkdir mnt/boot
mount /dev/sdb1 /mnt/boot
pacstrap /mnt base
genfstab -U /mnt >> /mnt/etc/fstab
lsblk --discard
    if DISC-GRAN and DISC-MAX not null trim support
```
