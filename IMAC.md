# IMAC (2008)

## Download and install B43 wireless driver for old Broadcom boards

```bash
$ wget http://bues.ch/b43/fwcutter/b43-fwcutter-018.tar.bz2 \ http://bues.ch/b43/fwcutter/b43-fwcutter-018.tar.bz2.asc
$ gpg --verify b43-fwcutter-018.tar.bz2.asc
$ tar xjf b43-fwcutter-018.tar.bz2
$ cd b43-fwcutter-018
$ make
# make install
$ cd ..
$ export FIRMWARE_INSTALL_DIR="/lib/firmware"
$ wget http://www.lwfinger.com/b43-firmware/broadcom-wl-5.100.138.tar.bz2
$tar xjf broadcom-wl-5.100.138.tar.bz2
# b43-fwcutter -w "$FIRMWARE_INSTALL_DIR" broadcom-wl-5.100.138/linux/wl_apsta.o
```

## Install Fancy - Fan Control (only for iMac Early 2008)

ArchLinux does not control the fan of (older) iMacs by default. I wrote a simple program in rust that does that.

**Note**: Needs rust dev environment installed. I may add a PKGBUILd for the AUR one das.

```bash
$ git clone https://github.com/aspera-non-spernit/fancy
>>> build and install
$ cd ..; rm -rf ./fancy
```
