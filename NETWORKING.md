# Local (encrypted) DNS caching

```bash

# pacman -S dnscrypt-proxy
# nano /etc/dnscrypt-proxy/dnscrypt-proxy.toml
>>>>
    // global settings
    listen_addresses = ['127.0.0.1:53', '[::1]:53']
    max_clients = 512
    ipv4_servers = true
    ipv6_servers = true
    dnscrypt_servers = true
    doh_servers = false
    require_dnssec = false
    require_nolog = false
    fallback_resolver = '9.9.9.9:53' // google in case everything fails
    forwarding_rules = '/etc/dnscrypt-proxy/forwarding-rules.txt'

    // optional testing

    [query_log]
    file = '/var/log/dnscrypt-proxy/query.log'

    [static]
    [static.'securedns.eu']
    stamp = 'sdns:AQcAAAAAAAAAEzE0Ni4xODUuMTY3LjQzOjUzNTMg9J8sc01itoYxntB-aRlDOy8ThfQe-8ovF21ZCy5FPoYcMi5kbnNjcnlwdC1jZXJ0LnNlY3VyZWRucy5ldQ'
    
# nano /etc/dnscrypt-proxy/forwarding-rules.toml
>>>>
    // enter url of captive portals and IP from "DNS/Gateway" from NetworkManager
    // hope it works.
    // example entries
    telekom.portal.fon.com 172.17.2.1
    unifi.bibfk.de:8880 172.16.16.1

# nano /etc/NetworkManager/NetworkManager.conf
>>>>
    [main]
    dns=none  // disable name resolver of NetworkManager

# nano /etc/resolv.conf
>>>>
nameserver=::1
nameserver=127.0.0.1
options=edns0

// optional (should not be necessary if NetworkManager is the only program to access /etc/resolv.conf)
# chattr -i /etc/resolv.conf
```