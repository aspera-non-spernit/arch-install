Where this guide begins:

You have successfully booted into Arch Linux from a .iso USB-Drive

# Legend

Some help for this guide:

- # indictes you have to execute the command as root or sudo
- $ means you should execute the command as non-root user
- >>> means you entered a file or program, and you have to enter text or hit the keys what comes after >>>
- {..} is a placeholder inside the { } indicates what you have to enter instead of the placeholder
- During this installation nano is used as Editor. To save a file CTRL+X, Y

# Before the installation

**Note:**  ```# ls /usr/share/kbd/keymaps/``` lists all available KeyMaps.

1. Set the keyboard layout
1. Check if UEFI is enabled. If directory doesn't exist , it's not enabled. This installation will not succeed.
1. Check if network connection is available

```bash
# loadkeys {KEYMAP}
# ls /sys/firmware/efi/evivars
# ping archlinux.org
```

## Partition disk with:

This will create two partitions:

1. boot partition
1. root partition (aka "/")

**Note: Assumed a single hard drive**

```bash
# fdisk /dev/sda
>>>
    g, n, ENTER, +256M, n, ENTER, ENTER, T, 1, 0, T, 2, 80, W
```

## Format partitions

```bash
# mkfs.ext4 /dev/sda2
# mkfs.vfat -F 32 /dev/sda1
```

## Mount partitions to your live system (arch iso)

**Note:** Do not change the order of the mounting. Mount /mnt before /mnt/boot!

```bash
# mount /dev/sda2 /mnt
# mkdir /mnt/boot
# mount /dev/sda1 /mnt/boot
```

## Rank download mirrors to speed up the download of packages

```bash
# cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.old
# rankmirrors /etc/pacman.d/mirrorlist.old > /etc/pacman.d/mirrorlist 
```

# Installation of Arch

## Install Arch linux

```bash
# pacstrap /mnt base
```

## Generate the fstab

```bash
# genfstab -U /mnt >> /mnt/etc/fstab
```

## Log into the installed system 

```bash
# arch-chroot /mnt
```

## Install amd/intel microcode

**Note:** If you have an Intel CPU

```bash
# pacman -S intel-ucode
```

**Note:** If you have an AMD CPU

```bash
# pacman -S intel-ucode
```

## Setting region and Language

1. Set the timezone (ls /usr/share/zoneinfo/ to list regions and cities) 
1. Uncomment (remove the # before the lines) for the region that's suitable for you.

```bash
# ln -sf /usr/share/zoneinfo/{REGION}/{CITY} /etc/localtime
# nano /etc/locale.gen
>>>
    en_US.UTF-8 UTF-8
    de_DE-UTF8 UTF-8

# locale-genfstab
# echo "KEYMAP={KEYMAP}" > /etc/vconsole.conf
# echo "{HOSTNAME}" > /etc/hostname
```

```bash
# passwd
>>>
    {PASSWORD} ENTER
    {PASSWORD} ENTER
```

## Make sure systemd updates automatically

**Note:** This step is very important. If you don't do it right, the system may not boot after an upgrade!

```bash
    # nano /etc/pacman.d/hooks/systemd-boot.hook
>>>
[Trigger]
Type = Package
Operation = Upgrade
Target = systemd

[Action]
Description = Updating systemd-boot...
When = PostTransaction
Exec = /usr/bin/bootctl update
```

## Config the systemd-boot bootloader

**Note: This step is very important. If you don't do it right, your system might not boot!

The command ```echo $(blkid...)``` copies the UUID of your partition to the arch.conf file you are modifiying in this 
section.

Take the UUID, in the last line and put it into the the part: ```options root=PARTUUID={...} ...```. Delete the last line 
afterwards.

```bash
# nano /boot/loader/loader.conf
>>>
    default arch
    timeout 0
    editor 0
# touch /boot/loader/entries/arch.conf
# echo $(blkid -s PARTUUID -o value /dev/sda2) >> /boot/loader/entries/arch.conf
# nano /boot/loader/entries/arch.conf
>>>
    title arch
    linux /vmlinuz-linux
    initrd {/amd-ucode.img OR /intel-ucode.img}
    initrd /initramfs-linux.img
    options root=PARTUUID={...} add_efi_memmap rootfstype=ext4 rw quiet loglevel=0
```

## Finalising the Installation

Your installation is complete, you can exit from the chroot environment and unmount and remove your .iso USB Drive. 

```bash
# exit
# umount -R /mnt
# reboot
```
Remove USB stick. 


## Login 

If you are not using a English /US keyboard you may enter wrong characters, use the screen keyboard

## Change region and language for the gdm (login screen)

***Note there is a keyboad layout for Gnome and for the Login screen (gdm)

```bash
Window key > Search Settings > Region & Language > Login
add your keyboard layout () German / German
```

```bash
$ curl https://sh.rustup.rs -sSf | sh
>>> 
    ENTER

$ source $HOME/.cargo/env
```

