# SECURITY

These are just a few things to make your system safer.

## Hardened Linux Kernel (optional)

```bash
# pacman -S linux-hardened
// add new loader to /boot/loader/entries/

```
## Enable Uncomplicated Firewall (ufw installed with gufw) (optional)

```bash
# systemctl enable ufw
# ufw deny
# ufw allow from 192.168.0.0/24
# ufw enable
# restart
```

## Add a non-root user

Never use the system as root, if not needed.

```bash
# useradd -m -U {USERNAME}
# passwd {USERNAME}
>>>
    {PASSWORD}
    {PASSWORD}
```

## Disable root login via ssh

After this you cannot login as root from another computer. If you have a home network, you have to login as the user you created 
before and then ```su``` to your root account.

```bash
# nano /etc/ssh/sshd_config
PermitRootLogin no
```

## Automatic SSH logout (timeout)
```bash
# nano /etc/profile.d/shell-timeout.sh
>>>
TMOUT="$(( 60*10 ))";
[ -z "$DISPLAY" ] && export TMOUT;
case $( /usr/bin/tty ) in
	/dev/tty[0-9]*) export TMOUT;;
esac
```

or 
 
```bash
$ export TMOUT="$(( 60*10 ))";
```

## Make your new user an administrator (optional)

If you want to allow your user to do administrative things. Actually needs more configuration to make it safe. I recommend to login 
as root before administrating and not use sudo.

1. You need to install the program sudo
1. Tell the system that {USERNAME} is allowed to do thing as root.

```bash
# pacman -S sudo
# EDITOR=nano visudo
>>>
    ## Cmnd alias specification
    Cmnd_Alias INSTALL=/usr/bin/makepkg -si, /usr/bin/pacman -S *, /usr/bin/pacman -Rns *
    Cmnd_Alias UPDATE=/usr/bin/pacman -Syu
    {USERNAME} {HOSTNAME} = INSTALL
    {USERNAME} {HOSTNAME} = UPDATE
```
Now you can execute all root command with ```$ sudo ...```

## Restrict Login attempts

```bash
# nano /etc/pam.d/system-login
>>>
auth optional pam_faildelay.so delay=4000000
auth required pam_tally2.so deny=3 unlock_time=600 onerr=succeed file=/var/log/tallylog
```

## Limit running processes
```bash
# nano /etc/security/limits.conf
>>>
* soft nproc 100
* hard nproc 200
```

## Permanent nano as visudo editor

Only use ```EDITOR=nano visudo``` once to make these settings, then edit ```/etc/sudoers``` with ```# visudo```

```bash
# EDITOR=nano visudo
>>>
Defaults editor=/usr/bin/rnano
```

## Hardening Kernel

### TCP/IP stack hardening

```bash 
// TCP SYN cookie protection
net.ipv4.tcp_syncookies = 1
// TCP rfc1337
net.ipv4.tcp_rfc1337 = 1
// reverse path filtering
net.ipv4.conf.default.rp_filter = 1
net.ipv4.conf.all.rp_filter = 1
// logging martian packets
net.ipv4.conf.default.log_martians = 1
net.ipv4.conf.all.log_martians = 1
// disable ICMP redirects
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.conf.default.secure_redirects = 0
net.ipv6.conf.all.accept_redirects = 0
net.ipv6.conf.default.accept_redirects = 0
//  disabling ICMP redirects on a non router
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0
// disable icmp echo requests (may cause problems with monitoring tools)
net.ipv4.icmp_echo_ignore_all = 1
net.ipv6.icmp.echo_ignore_all = 1




```
