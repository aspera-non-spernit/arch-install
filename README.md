# Collection of Installation Guides

## Pre-Installation and (opinionated) Installation of Arch Linux

[ARCH_INSTALL.md](ARCH_INSTALL.md)

## Additional software required if installed on an IMac 2008

[IMAC.md](IMAC.md)

## Security related post-installation steps

[SECURITY.md](SECURITY.md)

## Post-Installation 

[POST_INSTALL.md](POST_INSTALL.md)

## Optional Software Installation

[OPTIONAL.md](OPTIONAL.md)

## Networking / Privacy

[NETWORKING.md](NETWORKING.md)



