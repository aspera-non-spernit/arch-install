# OPTIONAL

Your Arch Installation should boot now. In the next steps additional optional programss will be installed.

## Gnome the Desktop environment

If you are logged in as the user you created, you either have to do ```$ sudo pacman ..``` or login as root ```su <ENTER> 
<PASSWORD> <ENTER>```


```bash
# pacman -S gnome xorg-init libinput gnome-tweak-tool xorg-server-xwayland \
    networkmanager git dhclient network-manager-applet wget gufw
```

# Create gnome start script (if you do not use gdm)

The step before installed Gnome. To manually start Gnome from the command line a start script should be created.
After this you should be able to start the desktop environment with ```$ startw```

```bash
# echo "XDG_SESSION_TYPE=wayland gnome-session" > /usr/local/bin/startw
# chmod 755 /usr/local/bin/startw
```

##  gdm "The Loging Screen" (optional)

If you want to see a graphical login screen after booting, install gdm and enable the gdm.service:

```bash
# pacman -S gdm
# systemctl enable gdm
´´´

## Enable the NetworkManager for Gnome

If the NetworkManager is not enabled you may not be able to access the Internet under Gnome. It has been installed in the previous 
steps, if not ```# pacman -S NetworkManager```

```bash
# systemctl enable NetworkManager
```



## Add aliases to .bashrc (optional)

Make the bash colorful and delete history when logging out

```bash
alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
alias la='ls -la --color=auto'
alias exit="history -c && echo "" > ~/.bash_history && exit"
```

# Specific for certain machines (ie. old iMac)

The steps below need compile tools:

```bash 
# pacman -S base-devel
```

## Install preload to speed up the starting of applications

```bash
$ git clone https://aur.archlinux.org/preload.git
$ cd preload
$ makepkg -si 
>>>
    {PASSWORD} 
    ENTER
    ENTER

cd ..; rm -rf ./preload
```

## Systemctl / Kernel Hardening 

```bash 
// enable TCP fast open (server)
net.ipv4.tcp_fastopen = 3

// small periodic system freezes
vm.dirty_background_bytes = 4194304
vm.dirty_bytes = 4194304
```

## Install the latest Nightly version of Firefox

```bash
$~ mkdir Development
$ git clone https://aur.archlinux.org/firefox-nightly.git
$ cd firefox-nightly
$ makepkg -si
    {PASSWORD}

>>> ERROR "public key not found"...
$ gpg --receive-keys {KEY}
$ makepkg -si

>>>
    {PASSWORD}
    {PASSWORD}

$ cd ..; rm -rf ./firefox-nightly
```

## Install the Gnome-Firefox Shell Extension to install Gnome Extionsions
```bash
$ git clone https://aur.archlinux.org/chrome-gnome-shell-git.git
$ cd chrome-gnome-shell-git
$ makepkg -si
>>>
    {PASSWORD} ENTER
    ENTER
    ENTER
    ENTER

$ cd ..; rm -rf firefox-nightly



Firefox visit:
>>>
    https://extensions.gnome.org/
    Click here to install browser extension OK, ADD
    restart firefox
    Customize > Uncheck Title Bar
    Customize > Dark Theme
    Visit: https://extensions.gnome.org/extension/307/dash-to-dock/
    ON 

TweakTool
    Global Dark Theme
    Extensions > DashToDock

    pacman -S virtualbox
